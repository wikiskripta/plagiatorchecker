<?php

//ob_end_flush();
ob_start();
$timerz = array();
$timerz[] = 0;
$debugstring = "";
unset($debug);
date_default_timezone_set('Europe/Prague');

require_once("htmlparser.php");

if (setunknownvar('debug',$_GET)) {
    if ($_GET['debug'] == "1") { $debug = true; }
}
else {
    $debug = false;
}

if ($debug) { $debugstring = "?debug=1"; }

//$debug = true; # comment-out in production version


if ($debug) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT);
    ini_set('error_reporting', E_ALL ^ E_NOTICE ^ E_STRICT);
}
else {
    error_reporting(E_ERROR ^ E_WARNING ^ E_PARSE);
    ini_set('error_reporting', E_ERROR ^ E_WARNING ^ E_PARSE);
}

//delete all debug files


if (@opendir("debug/") !== false) {
    $filestodelete = glob("debug/*.txt");
    if (count($filestodelete) > 0) {
        foreach ($filestodelete as $ff) {
            @unlink($ff);
        }
    }
}


//----------EDITABLE VARIABLES--------
set_time_limit(15); #maximum timeout from google
$timetoconnect = 0; #in seconds. How much time to allow connecting to google before aborting. 0 = indefinetely
$timetoreceive = 10; #in seconds. Maximum time the communication with google will take place for each phrase check.
$threshold = 0.4; //percentage (Max < 1) of how smaller the number of words in  last chunk is allowed to be. The highest, the more relaxed. 
$notfound = array( #the strings that indicate no results in google's search results
    "no results",
    "did not match any documents"
    ); 
$delay = 4; #delay between each google search in seconds
$useragentfile = 'useragents.txt'; #relative path to user-agents txt file
$proxyfile = 'proxies.txt';
$retrylimit = 1;
$clients = array("chrome","safari","opera","firefox");
$pr = false;
//------------------------------------


//------DON'T EDIT BELOW!-------------
$version = "2.5"; #script version
$input = ''; #initialization of the input string
$returninput = '';
$otherarticles = array();
setunknownvar('article',$_GET) ? $article = trim($_GET['article']) : $article = '';

//$article = "Classification_of_Receptors";

if ($article != "" && setunknownvar('input',$_POST) === false) {
    $article = str_replace(" ","_",$article);
    $articlehuman = str_replace("_"," ",$article);
    $returninput = getdataz($article);
    //echo "returninput length: ".strlen($returninput);
    //echo "<br/>".$returninput ? "success: $returninput" : "no";
    //file_put_contents("debug/returninput.txt",$returninput);
    $input = $returninput;
}
elseif (setunknownvar('input',$_POST) != "") {
    //echo "<br/>2nd if";
    $_POST['input'] ? $input = $_POST['input'] : $input =  $returninput;
}
elseif ($_POST['input'] == "" && $debug === true) {
    // do nothing continue with manual input
}
else {
    die('no input provided - exiting.');
}

if (!$debug) {
    setunknownvar('sensitivity',$_GET) ? $sensitivity = floatval($_GET['sensitivity']) : $sensitivity = 0.85;
    setunknownvar('searchall',$_GET) ? $searchall = $_GET['searchall'] : $searchall = 'yes';
    setunknownvar('breakevery',$_GET) ? $breakevery = intval($_GET['breakevery']) : $breakevery = 15;
    if ($breakevery < 10) { $breakevery = 10; }
}

if ($debug) {
    setunknownvar('sensitivity',$_POST) ? $sensitivity = floatval($_POST['sensitivity']) : $sensitivity = 0.85;
    setunknownvar('searchall',$_POST) ? $searchall = $_POST['searchall'] : $searchall = 'yes';
    setunknownvar('breakevery',$_POST) ? $breakevery = intval($_POST['breakevery']) : $breakevery = 15;
    echo "Sensitivity: $sensitivity <br/>";
    echo "Search all: $searchall <br/>";
    echo "Breakevery: $breakevery <br/>";
}

if (setunknownvar('breakevery',$_POST)) {
    if (intval($_POST['breakevery']) != 15 && intval($_POST['breakevery']) >= 7) {
        $breakevery = intval($_POST['breakevery']);
    }
}

$useragents = file($useragentfile);
$proxies = file($proxyfile);

print '<html>
<head>
<script type="text/javascript" src="motionpack.js"></script>
<script type="text/javascript">
function showhide(el) {
    var a = document.getElementById(el);
    var b = a.style.display;
    if (b == "none") { slidedown(el); }
    else { slideup(el); }
}
</script>
<style>
body {
    font-family: verdana !important;
    font-size: 10pt !important;
}
#results {
    border-width: 1px;
    border-style: solid;
    border-color: black;
    border-radius: 5px;
    font-family: verdana !important;
    font-size: 10pt;
}
#results td {
    border-collapse: collapse;
    border-width: 1px;
    border-style: dotted;
    border-color:black;
    overflow:hidden;
    font-family: verdana;
    font-size: 10pt;
}
#inputx {
    border-width: 1px;
    border-style: solid;
    border-color: black;
    border-radius: 5px;
    font-family: verdana;
    font-size: 10pt !important;
}
#inputx td {
    border-collapse: collapse;
    border-width: 1px;
    border-style: solid;
    border-color:black;
    overflow:hidden;
    font-family: verdana;
    font-size: 10pt;
}
</style>'
;
print "
        <script type='text/javascript'>
        var image_width = 748;
        var prog_value = 0;
        function DisplayProgress(prog_value,pic_id,label_id,percent_value,curcount,totalcount,timeleft) {
            prog_value = Math.round(prog_value * image_width);
            //alert(prog_value);
            document.getElementById(pic_id).style.width = prog_value + 'px';
            document.getElementById(label_id).innerHTML = percent_value + '%' + ' (' + curcount + '/' + totalcount + ') - Estimated time left: ' + timeleft; 
        }
        </script>
</head>
<body>
";
print "<div style=\"width:100%; margin:auto; text-align:center;font-size: 16pt;\"><b>PLAGIARISM CHECKER - v$version </b><span style=\"float:right;font-size:8pt;\"><a href='mailto:paris.charilaou@lf1.cuni.cz'><i>Send Feedback</i></a></span></div><br/>";
print '<div style="width:100%; margin:auto; text-align:right;font-size: 10pt;"><i>Copyright &copy; Paris Charilaou 2011-2014</i></div>';


if ($debug) {
print '<br/>
<div style="width:100%; margin: auto; text-align: center;">
<form id="formx" name="formx" method="post" action="copyvio-checker-wiki.php'.$debugstring.'">
<table id="inputx" align=center>
<tr>
<td style="vertical-align: center">Input text here:</td><td colspan=2 style="vertical-align: center"><textarea name="input" id="input" cols="200" rows="10">'.$input.'</textarea></td>
</tr>
<tr>
<td width=33%>Break every: <select name="breakevery" id="breakevery">'.makeselectbox().'</select> words</td><td width=33%>Use <select name="searchall" id="searchall">'.makeselectbox(1,10).'<option selected=selected value="yes" onclick="return prompt(\'This will make the search too slow or even impossible for a big document. Do you wish to continue?\');">ALL</option></select> phrases</td><td width=33%>Accept only at least from: <select name="sensitivity" id="sensitivity"><option value="0.65">65%</option>\n<option value="0.70">70%</option>\n<option value="0.75">75%</option>\n<option value="0.80">80%</option>\n<option value="0.85" selected=selected>85%</option>\n<option value="0.90">90%</option>\n<option value="0.95">95%</option></select> relative sensitivity match</td>
</tr>
<tr>
<td colspan=3 style="text-align:center; vertical-align: center">
<input type="submit" name="submit" id="submit" value="Submit" />
</td>
</tr>
</table>
</form>
</div>
';
}

print "
<div style='width:100%; margin: auto; text-align: left;'>
<span style='font-size: 12pt;float:left;'><b>Checking article: </b><i>$articlehuman</i></span><span style='float:right;'><img src='poweredbygoogle.gif'/></span><br/><br/>".printotherarticles()."
<hr style='width:100%; border: 1px solid grey; border-radius:10px; box-shadow: 0px 1px 2px silver;' />
<div style='clear: both;margin: auto auto 20px auto;width:753px;'>
    <div style='border-style: ridge; border-width: 3px;float: left;width: 747px;height: 27px;z-index: 5;position: relative;top: 0;'>
           <img id='progbar' src='progressbar-nu.png' style='float:left; height:29px; width: 0px; position: relative; padding-left: 1px; padding-top: 0px; z-index: 0;' />
           
    </div>
    <div id='progresslabel' style='width: 747px;text-align:center;'></div>
</div>
";


/*
echo "
Set sensitivity: $sensitivity <br/>\n
Set searchall: $searchall <br/>\n
Set breakevery: $breakevery <br/>\n
";
*/

function makeselectbox($min=12,$max=30) {
    $out = "";
    for($i=$min;$i<=$max;$i++) {
        $selected='';
        if ($i == $min) { $selected = "selected=selected"; }
        $out .= "<option value='$i' $selected>$i</option>\n";
    }
    return $out;    
}

function unichr($intval) {
return mb_convert_encoding(pack('n', $intval), 'UTF-8', 'UTF-16BE');
}
function unbullet($input) {
   /* $find = array(
        "-	","o	","•	","•	","→","•	",
chr(145),
chr(146),
chr(147),
chr(148),
chr(151),
chr(150),
chr(133),
chr(149),
chr(226),
chr(9),
unichr("8594"),
"[edit]",
" |",
"| ",
" |-",
"|-",
"{|",
"|}",
"class=\"wikitable\""
);
    /* $replace = array (
      "","","","",""  
    );*/
    /*
    $replace = "";
    $output = str_ireplace($find,$replace,$input);
    $find = array (
    //  chr(10).chr(13),
    //  chr(13).chr(10),
      chr(10),
      chr(13)
    );
    $replace = " ";
    $output = str_ireplace($find,$replace,$output);
    $find = "    ";
    $replace = " ";
    $output = str_ireplace($find,$replace,$output);
    //$aaa = stripos($output,unichr("8594"));
    $output = mb_ereg_replace (" → ", " " , $output);
    $expr = '/[0-9](.|\))(\\t| |)/i';
    $output = preg_replace($expr,"",$output);
    //echo $aaa;
    */
    $output = str_replace("\n"," ",$input);
    //file_put_contents("debug/nobreaks.txt",$output);
    return $output;
}

function breakup($text, $cnt = 10)  {
    
    global $threshold;
    $removeif = round($cnt-($cnt*$threshold),0);
    //echo $removeif;
    $text = unbullet($text);
    $result = array();
    $sentence = '';
    
    $words = explode(' ', $text);
    $chunks = array_chunk($words,$cnt);
    foreach ($chunks as $k=>$v) {
        $sentence = implode(" ",$v);
        $sentence = trim($sentence);
        $result[] = $sentence;
    }
    foreach ($result as $key=>$val) {
        $w = explode(' ',$val);
        $c = count($w);
        if ($c < $removeif) {
            unset($result[$key]);
        }
    }
    
    /*echo "<pre>";
    print_r($result);
    echo "</pre>";*/
    return $result;

}

unset($segmentzold);
$segmentzold = array();
$discarded = array();
$invalidsegments = 0;

$inittime = round(microtime(true),4);

if ($input != "") {
    //echo "<br/><br/>starting...<br/>";
    $segmentz = breakup($input,$breakevery);
   
    foreach ($segmentz as $k=>$v) {
        $asd = explode(" ", $v);
        if (count($asd) < $threshold) {
            $discarded[$k]['phrase'] = $v;
            $discarded[$k]['reason'] = "Phrase is under $threshold words.";
            unset($segmentz[$k]);
            $invalidsegments++;
        }
    }
    $segmentzold = $segmentz;
    if ($searchall != "yes") {
        $tempcount = count($segmentz);
        if (intval($searchall) >= $tempcount) {
            $searchall = $tempcount;
        }
        $selected = array_rand($segmentz,intval($searchall));
        /*echo "about to print selected clauses: <br/>\n<pre>";
        print_r($selected);
        echo "</pre><br/>\n";*/
        $segmentz = array();
        if (is_array($selected)) {
            foreach ($selected as $v) {
                $segmentz[] = $segmentzold[$v];
            }
        }
        else {
            $segmentz[] = $segmentzold[$selected];
        }
    }

    $totalqueries = count($segmentz);
    $totalloops = $totalqueries;
    $percent_per_loop = 100 / $totalloops;

    
    print "<div style='clear:both;margin-top:35px;'>Total queries (phrases) to be submitted: <b>$totalqueries</b></div><br/>\n";
    ob_flush();
    flush();
    /*
    echo "<pre>";
    print_r($segmentz);
    echo "</pre>";
    */
    $urlz = array();
    $output = array();
    $linksfound = array();
    $progcount = 0;
    $percent_last=0;
    $notchecked = array();
    
    
    foreach ($segmentz as $key=>$val) {
        $retry = 0;
        $lasttime = round(microtime(true),4);
        //echo "STARTING WITH SEGMENT #$key <br/>\n";
        restart:
        $query = urlencode('"'.$val.'"');
        $client = $clients[array_rand($clients)];
        $url = "https://www.google.com/search?hl=en&filter=0&ie=UTF-8&safe=0&client=$client&q=$query";
        if ($url2 != "") { $url = $url2; $url2 = ""; } //get the 302.moved URl from google in case of "restart" triggered
        //$a = file_get_contents($url);
        //echo "about to run curl...";
       
        $a = resultloader($url,$pr);
        @file_put_contents("debug/output-all-$key.txt",$a);
       
        $zzz = mb_stripos($a,'<div id="ires">');
        //echo "zzz (1st): $zzz<br/>";
        if ($zzz === false) {
            $pattern = '/id="?i?res"?.?><ol/im'; #<div id="[i|]res">
            $zzz = preg_match($pattern,$a,$matches,PREG_OFFSET_CAPTURE);
            if ($zzz > 0) {
                //echo "<pre>".print_r($matches,true)."</pre>";
                $zzz = $matches[0][1];
                //echo "Found pregmatch @: $zzz <br/>";
            }
        }
        $noresultsfound = striposa($a,$notfound);
        //$noresultsfound = stripos($a,$notfound);
        if ($noresultsfound !== false) {
            $discarded[$key]['phrase'] = $val;
            $discarded[$key]['reason'] = "Phrase returned no matches.";
            //unset($segmentz[$k]);
            $invalidsegments++;
            goto skipper;
        }
        //echo "ires found @ $zzz , ";
        $aaa = stripos($a,"<ol ",$zzz);
        //echo "begginer found @ $aaa , ";
        unset($usedstripos);
        $bbb = preg_match("/<\/ol>.*<\/div>.*<!--z-->/i",$a,$matchez,PREG_OFFSET_CAPTURE,$aaa);
        if ($bbb > 0) {
            $bbb = $matchez[0][1];
            $usedstripos = false;
        }
        else {
            $bbb = stripos($a,"</ol></div><!--z-->",$aaa);
            $usedstripos = true;
        }
        //echo "ender found with pregmatch @ $bbb <br/>";
        $c = substr($a, $aaa, $bbb-$aaa+5);
        //$tt = $key+1;
        unset($written);
        if (@file_put_contents("debug/output-$key.txt",$c)) { $written = true; }
        if (strlen($c) < 50) { #exit if google doesn't return response
            echo $written ? "c written successfully<br/>" : "c failed to be written<br/>";
            echo "Found pregmatch (div id=res) [zzz] @: $zzz";
            echo $usedstripos ? " (stripos was used. pregmatch failed)<br/>" : " (pregmatch succeeded!)<br/>";
            echo "begginer found (<ol) [aaa] @ $aaa , ";
            echo "ender found with pregmatch (</ol>) [bbb] @ $bbb <br/>";
            echo "C length: ".strlen($c)."<br/><br/>";
            $newa = preg_replace("/(<textarea.*?<\/textarea>)/im","",$a,-1,$acount);
            if ($acount > 0) { $a = $newa; }
            echo "<br/><b>WARNING:</b> no response received from Google. Probable block. Please try again later<br/>";
            echo "Currently checking phrase #$key: $val";
            echo "<br/><br/>Google returned:<textarea>";
            echo $a;
            echo "</textarea><br/>";
            if (strlen($a) > 0 && stripos($a,"<TITLE>302 Moved</TITLE>") === false) { echo "<br/>Aborting...<br/>"; die("<b>Script ended!</b>"); }
            if (strlen($a) == 0 or stripos($a,"<TITLE>302 Moved</TITLE>") !== false) {
                $pr = true;
                echo "<br/>About to retry: ";
                preg_match('/(?<=<A HREF=")(.*)(?=">)/is',$a,$mat);
                $url2 = $mat[0];
                if ($retry == $retrylimit) {
                    $kkey = $key +1;
                    echo "Empty response again...retry limit reached! skipping to phrase #$kkey<br/>";
                    $retry = 0;
                    $notchecked[$key] = $val;
                    goto skipper;
                }
                elseif ($retry < $retrylimit) {
                    $retry++;
                    echo "Empty response...retrying<br/>";
                    sleep($delay);
                    goto restart;
                }
            }   
        }
        //file_put_contents("debug/output-raw.txt",$a);
                
        $counter = array();
        //echo "C is: ".strlen($c) ."<br/>\n";
        $currentarrayofresults = array();
        $currentkey = $key;
        $currentphrase = $val;
        $newhaystack = $c;
        
        /*
        the_time("before calling finderz() for segment #$key");
        ob_flush();
        flush();
        */
        
        finderz("<em>","<!--n--></li>",$c);
        /*
        the_time("after calling finderz() for segment #$key");
        ob_flush();
        flush();
        */
        
        if ($c != $newhaystack) { $c = $newhaystack; }
        /*echo "<pre> Printing counter array:<br/>\n";
        print_r($counter);
        echo "</pre>";*/
        $res = array();
        $x = 0;
        //echo "Counter array count: ".count($counter) ."<br/>\n";
        $output[$key]['phrase'] = $val;
        /*
        foreach ($counter as $k=>$v) {
            $x++;
            $dataz = explode("|",$v);
            $from = $dataz[0];
            $to = $dataz[1];
            //echo "adding new segmentz result...($k)...($v)....";
            echo substr($c,$from,$to-$from)."<br/>";
            $res = boldfinder(substr($c,$from,$to-$from),$sensitivity,"<em>");
            //echo "Segmentz key: " . $segmentz[$key] . "<br/>\n";
            //echo "Segmentz key-key: " . $segmentz[$key][$k] . "<br/>\n";
            //print_r($res);
            //$segmentz[$key]['matches']['rawphrase] = $val;
            $output[$key]['matches'][$k] = $res;
            //$segmentz[$key] = $res; // = $res['phrase'];
            //$segmentz[$key][$k]['sensmatch'] = $res['sensmatch'];
            //$segmentz[$key][$k]['sensmatchrel'] = $res['sensmatchrel'];
            //$segmentz[$key][$k]['matched'] = $res['matched'];
            
            //echo "results counter ($k) added to segment ($key)!<br/>\n";
        }
        */
        //echo "finished with segment ($key)<br/>\n";
        // progress bar
        skipper:
        sleep($delay);
        $newtime = round(microtime(true),0);
        $timeleft = time_estimator($newtime);
        $progcount++;
        if ($progcount <= $totalloops) {
            $percent_now = round($progcount * $percent_per_loop);
            if($percent_now != $percent_last) {
                $difference = $percent_now - $percent_last;
                    for($j=1;$j<=$difference;$j++) {
                        $percent_nowx = $percent_now / 100;
                        print "<script type='text/javascript'>DisplayProgress(".$percent_nowx.",'progbar','progresslabel',".$percent_now.",".$progcount.",".$totalloops.",'".$timeleft."');</script>\n";
                    }
                $percent_last = $percent_now;
            ob_flush();
            flush();
            }
        }
        //------------
    }
    
    /*echo "<br/>links collection:<pre>";
    print_r($urlz);
    
    echo "About to print segmentz:<br/>\n";*/
    //echo "<pre>";print_r($output);echo "</pre>";
    //$out = consolidate();
    //echo "<br/><br/>"; #\n\n\nFINAL OUTPUT:<BR/><BR/>\n\n";
    $finaloutput = filtermatched($output); //consolidate();
    /*echo "About to print segmentz:<br/>\n";
    print_r($finaloutput);
    echo "</pre>";*/
    echo makeui();
    $countdiscarded = count($discarded);
    echo "<br/><br/>Phrases with absolutely no match: <b>$invalidsegments</b><br/><b>Total discarded phrases: $countdiscarded</b> that failed to match: <span style='font-size: 7pt;cursor: pointer;' onclick='showhide(\"discdetails\");'>(Click to show/hide listing)</span><BR/><BR/>\n\n<div id='discdetails' style='display:none; font-size:9pt;'>";
    /*
    echo "<br/><pre>";
    print_r($discarded);
    echo "</pre>";
    */
    foreach ($discarded as $disc=>$discx) {
        /*echo "about to print discx:<br/><pre>";
        print_r($discx);
        echo "</pre>";*/
        //$sensdisc = round(100*$discx['matches']['sensmatchrel'],2)."%";
        $notcheckedcolor = '';
        if (isset($notchecked[$disc])) {
            $notcheckedcolor = "color: red;";
            $discx['reason'] = "Google returned no response; phrase was skipped.";
        }
        $outz = $discx['phrase'];
        /*if ($discx['reason'] == "") { $outz .= " <b>(Relative sensitivity match: $sensdisc)</b>"; }
        else {*/
            $outz .= " </i><b style='$notcheckedcolor'>Reason: {$discx['reason']}</b>";
        //}
        echo "<b>ID #$disc</b>: <i>$outz<br/>\n";
    }
    echo "</div>";
    //print_r($out);
    //echo "</pre>";
}

function makeui() {
    global $finaloutput, $urlz,$notchecked;
    $html = "";
    $html .= "<table id='results' width=100%><tr><td colspan=3>Results</td></tr>";
    $html .= "<tr><td style='text-align:center;'><b>Input phrase</b></td><td style='text-align:center;'><b>Matched Phrase</b></td><td style='text-align:center;'><b>Sensitivity</b></td></tr>";
    $sentences = array();
    $getmatches = $finaloutput;
    $countsegments = count($getmatches);
    $totalsens = 0;
    $totalsensrel = 0;
    foreach($getmatches as $key=>$val) {
        $phrasesens = 0;
        $phrasesensrel = 0;
        $inputphrase = $val['phrase'];
        $matchdetails = $val['matches'];
        $countphrasematches = count($matchdetails);
        $segmentphrasehtml = "<hr style='border: 1px dashed black;' />";
        $sentences[$key]['phrase'] = $inputphrase;
        
        
        foreach($matchdetails as $k=>$v) { #run through matched entries of segment
            $matchedphrase = $v['bolds']['phrase'];
            //$getlink = $urlz[$key]['links'][$k];
            $getlink = $v['link'];
            $matchedphrase = str_ireplace("<em>","<a href='$getlink' target='_blank' style='color:red;curson:pointer;'>",$matchedphrase);
            $matchedphrase = str_ireplace("</em>","</a>",$matchedphrase);
            $sens = $v['bolds']['sensmatch'];
            $sensrel = $v['bolds']['sensmatchrel'];
            $phrasesens = $phrasesens + $sens;
            $phrasesensrel = $phrasesensrel + $sensrel;
            $sens = round(100*$sens,2)."%";
            $sensrel = round(100*$sensrel,2)."%";
            $segmentphrasehtml .= "<span style='font-style:italic;'>$matchedphrase</span> - Sensitivity: $sens (<b>$sensrel</b>)<hr style='border: 1px dashed black;' />";
            $sentences[$key]['source'] .= " ".$getlink." ";
        }
        
        $avgsensphrase = round(100*$phrasesens / $countphrasematches,2)."%";
        $avgsensrelphrase = round(100*$phrasesensrel / $countphrasematches,2)."%";
        $totalsens = $totalsens + $phrasesens / $countphrasematches;
        $totalsensrel = $totalsensrel + $phrasesensrel / $countphrasematches;
        $matchoutput = "<span style='cursor: pointer;' onclick='showhide(\"segmentmatches$key\");'>Total web matches: <b>$countphrasematches</b> [$avgsensphrase (<b>$avgsensrelphrase</b>)] <span style='font-size:7pt; float:right;'>Click to view matched phrases</span></span><div id='segmentmatches$key' style='margin-top:10px; overflow:visible;font-size:7pt;display:none'><br/>";
        $matchoutput .= $segmentphrasehtml;
        $matchoutput .= "</div>"; #close div for matched entries of segment
        $sensphrasehtml = "Absolute: $avgsensphrase , Relative: $avgsensrelphrase";
        $html .= "<tr><td style='font-size:8pt;' width=33%>(ID: #$key) $inputphrase</td><td style='font-size:8pt;' width=33%>$matchoutput</td><td style='font-size:8pt;' width=33%>$sensphrasehtml</td></tr>";
    }
    if ($countsegments >0) {
        $averagesens = round(100*$totalsens / $countsegments,2)."%";
        $averagesensrel = round(100*$totalsensrel / $countsegments,2)."%";
    }
    else {
        $averagesens = "0%";
        $averagesensrel = "0%";
    }
    $averagesenshtml = "Absolute: $averagesens , Relative: $averagesensrel";
    
    $forwiki = '';
    $html .= "<tr><td colspan=2><b>Total phrases with sufficient web match</b></td><td style='text-align:center;'><b>Average sensitivity</b></td></tr>\n
    <tr><td colspan=2>$countsegments</td><td style='text-align:center;'>$averagesenshtml</td></tr></table>";
    
    $html .= "<br/><br/><span onclick='showhide(\"fortalkpage\");'>To paste in the article's discussion page: <span style='font-size: 7pt;'>(click to toggle)</span></span><br/><br/>";
    $html .= "<div id='fortalkpage' style='display:none;font-size:9pt;'>The following phrases have been identified as plagiarism. Please rectify:<br/><code>";
    foreach ($sentences as $kk=>$vv) {
        $kkk = $kk+1;
        $forwiki .= "$kkk) <i>".$vv['phrase']."</i> (".$vv['source'].")<br/>";
    }
    $html .= "$forwiki</code></div>";
    
    return $html;
}


function checkpunct($haystack,$needle,$antineedle) {
    global $c;
    $punctuation = array (
    "," ,
    "." ,
    ";"
    );
    $antineedle = str_replace("/","\/",$antineedle);
    $ex = implode("|",$punctuation);
    $expr = "/".$antineedle."($ex)( |)".$needle."/i";
    //echo "expression check: $expr \n";
    $haystack = preg_replace($expr,", ",$haystack);
    $c = preg_replace($expr,", ",$haystack);
    //file_put_contents("debug/output-cor.txt",$haystack);
    $haystack = str_replace(" , ",", ",$haystack);
    $haystack = str_replace(" . ",". ",$haystack);
    //echo "new haystack: $haystack \n\n\n";
    return $haystack;
}

function getgooglelink($input,$currentkey) {
    global $urlz;
    //echo "<br/>entered googlelink...";
    #check if books - level 2 or 4 in array
    $level = $input['level'];
    switch($level) {
        case 2;
        #books,slideshare,etc
        array_key_exists(0,$input['childNodes'][0]['childNodes']) ? $getter = $input['childNodes'][0]['childNodes'][0]['stratr'] : $getter = '' ;
        if ($getter == "" or stripos($getter,"href=") === false) {
            $getter = $input['childNodes'][1]['childNodes'][0]['stratr'];
            if (stripos($getter,"href=") === false) {
                $getter = $input['childNodes'][2]['childNodes'][0]['stratr'];
            }
        }
        break;
        /*case 3:
        #books,slideshare,etc
        $getter = $input[0]['childNodes'][0]['childNodes'][0]['stratr'];
        break;*/
        case 3:
        #not books
        $getter = $input['childNodes'][0]['childNodes'][0]['stratr'];
        if (stripos($getter,"href=") === false) {
            $getter = $input['childNodes'][0]['childNodes'][0]['childNodes'][0]['stratr'];
            if (stripos($getter,"href=") === false) {
                $getter = $input['childNodes'][1]['childNodes'][0]['stratr'];
                if (stripos($getter,"href=") === false) {
                    $getter = $input['childNodes'][2]['childNodes'][0]['stratr'];
                }
            }
        }
        break;
        case 4:
        //echo "<b>entered case 4</b> ";
        $getter = strip_tags($input['childNodes'][0]['childNodes'][1]['childNodes'][0]['stratr']);
        //echo $getter ."<br/>";
        break;
        case 5:
        $getter = $input['childNodes'][0]['childNodes'][1]['childNodes'][0]['stratr'];
        break;
    }
    //echo "Google link?: $getter <br/>";
    if (stripos($getter,'href="') !== false) {
        $getter = substr($getter,7);
        $aa = stripos($getter,'"');
        //echo "<br/>quote found @ $aa <br/>";
        $bb = strlen($getter)-$aa;
        //echo "<br/>extract $bb bytes";
        $getter = substr($getter,0,$aa);
        //echo "Processed google link for segment #$currentkey: $getter";
    }
    if (stripos($getter,'http://') === false) {
        $getter = "http://".$getter;
    }
    $urlz[$currentkey]['links'][] = $getter;
    return $getter; 
}


function finderz($needle,$needlez,$haystack,$starter=0,$a='',$antineedle='',$antistarter=0) {
    global $output,$currentarrayofresults, $currentkey, $currentphrase, $newhaystack, $debug,$sensitivity;
    $endnow = false;
    //echo "<br/><br/>Finder starting from: $starter for segment #$currentkey";
    if ($antineedle == '') {
        $antineedle = str_replace("<","</",$needle);
    }
    //echo "Antineedle: $antineedle \n\n";
    //echo "current starter pos: $starter \n\n";
    /*
    if ($a == '') { #first run
        //echo "this is first run <br/>\n";
        $haystack = checkpunct($haystack,$needle,$antineedle);
    }
    */
    //file_put_contents("debug/output-cor.txt",$haystack);
    $totalmatches = preg_match_all('/<li(\s\w+?[^=]*?="[^"]*?")*?\s+?class="(\S+?\s)*?g(\s\S+?)*?".*?>(.*?)<\/li>/i',$haystack,$res);
    echo $debug ? "Total matches returned by google for #$currentkey: $totalmatches <br/>" : '';
    $counter = 0;
    foreach ($res[0] as $key=>$val) {
        $counter++;
        $parsehtml = new htmlParser($val);
        $htmlarray = $parsehtml->toArray();
        $htmlarray = $htmlarray[0];
        if (stripos($val,"wikilectures.eu") !== false OR stripos($val,'<ul class="lsnip">') !== false) {
            echo $debug ? "skipping detected wikilectures or lsnip result...<br/>\n" : '';
            //$counter--;
            continue;
        }
        $output[$currentkey]['matches'][$counter]['alltext'] = $htmlarray['innerHTML'];
        $output[$currentkey]['matches'][$counter]['bolds'] = boldfinder($val,$sensitivity);
        $output[$currentkey]['matches'][$counter]['link'] = getgooglelink($htmlarray,$currentkey);
        echo $debug ? "<b>Result #$counter collected</b><br/>\n" : "";
        $divider = "\n\n=========MATCH #$counter==========\n\n";
        if ($debug) { @file_put_contents("debug/output-metadata-$currentkey.txt",$divider.print_r($output[$currentkey]['matches'][$counter],true)."\n\nHTML ARRAY\n\n".print_r($htmlarray,true),FILE_APPEND) ; }
    }
     
    /*
    $aa = stripos($haystack,"<li class=g",$starter);
    $bb = stripos($haystack,"<!--n--><!--m-->",$aa);
    if ($bb === false) {
        //echo "<br/>bb was false - couldnt find normal ending match...";
        $bb = stripos($haystack,"<!--n--></ol>",$aa); #reached the end
        $endnow = true;
        //file_put_contents("debug/output-XXX.txt",$haystack);
        //echo "<br/><br/>reached the end, grabbing the last result!<br/><br/>";
    }
    $cc = substr($haystack,$aa,$bb-$aa);
    $cc .= "</li>";
    //$rand = mt_rand(0,500);
    $rand = $aa;
    if ($endnow === true) { $rand = "last"; }
    //file_put_contents("debug/check-in$rand.txt",$cc);
    
    $parser = new htmlParser($cc);
    $currentarrayofresults = $parser->toArray();
    file_put_contents("debug/check-in-parse$rand.txt",print_r($currentarrayofresults,true));
    */
    /*
    $dd = true;
    if (stripos($cc,"wikilectures.eu") !== false OR stripos($cc,'<ul class="lsnip">') !== false)  {
        $dd = false;
        $jumper = stripos($haystack,"<!--n--><!--m-->",$starter+20);
    }
    */
    //echo $dd ? "DD is OK!<br/>" : "DD is false - wikilectures result found!<br/>";
    
    /*$aaa = stripos($haystack,"class=st>",$starter);
    if ($aa < $aaa) { $starter = $aaa; } #google books fix
    */
    #check that it doesnt jump result
    /*
    $jumper = stripos($haystack,"<!--n--><!--m-->",$starter+20);
    $a = stripos($haystack,$needle,$aa);
    $aaa = stripos($haystack,"class=st>",$aa);
    if ($aa < $aaa) { $a = stripos($haystack,$needle,$aaa); }
    $setoffset = 0;
    if ($endnow === false && $a > $jumper) { #the result was skipped!
        
        //echo "<br/>The result was skipped... fixing!";
        $fff = stripos($haystack,"<span class=st>",$starter) + 15;
        $ttt = stripos($haystack,"<span class=",$fff-15);
        $bind1 = substr($haystack,0,$fff);
        $bind2 = substr($haystack,$ttt+15,strlen($haystack));
        $haystack = $bind1 . "<em>" . $currentphrase . "</em>" . $bind2;
        $setoffset = strlen("<em>".$currentphrase."</em>");
        //file_put_contents("debug/newhaystack.txt",$haystack);
        $a = stripos($haystack,$needle,$aa);
    }
    
    if ($a === false && stripos($haystack,"scribd.com",$starter) !== false && $dd === true) {
        //echo "<br/>A was false!";
        $aaaa = stripos($haystack,"<li class=g",$starter);
        $bbbb = stripos($haystack,$needlez,$aaaa);
        getgooglelink($currentarrayofresults,$currentkey);
        $counter[] = $aaaa."|".$bbbb;
    }
    //echo $a ? "A: $a <br/>\n" : "A: SHIT!<br/>\n";
    //echo "New find at pos: $a ". substr($haystack,$a,50)  ."<br/>\n";
    elseif ($a !== false) {
        //echo "<br/>A: $a";
        $fromz = intval($a) + strlen($needle) -3 ;// + $setoffset;
        //echo "<br/>Fromz = $fromz";
        $b = stripos($haystack,$needlez,$fromz);
        if ($b === false && $endnow === false) {
            //echo "<br/>trying to set b ($b) without being in the final result...";
            $b = stripos($haystack,"<!--n--><!--m-->",$fromz);
        }
        if ($b === false && $endnow === true) {
            //echo "<br/>trying to set b while at the final result....";
            $b = stripos($haystack,"<!--n--></ol>",$fromz);
            //echo $b ? "b is ok now ($b)!" : "b is STILL FALSE!";
        }
        
        if ($b !== false) {
            //echo "$b <br/>\n";
            //echo $dd ? "<br/>dd is true" : "<br/>dd is FALSE";
            
            //echo "<br/>fixing b ($b)...";
            $oldb = $b;
            $fixb = false;
            $fixb = stripos($haystack,"<b>...</b>",$fromz);
            if ($fixb > $jumper && $endnow === false) { $fixb = false; }
            if ($fixb === false && $b != $fixb) {
                $fixb = stripos($haystack,'<span class="f"',$fromz);
                //echo "<br/>fixed b for custom result...fixb = $fixb";
            }
            if ($fixb !== false && $b != $fixb) {
                //echo "<br/>b fixed!";
                $b = $fixb;
                //echo "<br/>printing description ($a -> $fixb): <i>".substr($haystack,$a,$fixb-$a)."</i><br/>";
            }
            if ($dd === true) {
                $counter[] = $a."|".$b;
                getgooglelink($currentarrayofresults,$currentkey);
                }
            
            //echo substr($haystack,$a,$b-$a)."<br/>";
            $newstarter = $oldb + 1; # $starter+$b+strlen($needlez);
            $antistarter = stripos($haystack,$antineedle,$a);
            echo $debug ? "<br/>new starter pos: $newstarter<br/>" : "";
        }
        else {
            echo $debug ? "<br/>b is false! exiting finderz....<br/>" : "";
            $newhaystack = $haystack;
            return;
        }
        
        $xxxx = stripos($haystack,"<!--n--></ol>",$newstarter);
        $xxxz = stripos($haystack,"<li class=g",$newstarter);
        
        if ($xxxz !== false && $xxxx > $xxxz && $endnow === false) {
            $exitnow = false;
            echo $debug ? "<br/>still not exiting @ $newstarter <br/>" : "";
        }
        elseif ($xxxz === false) {
            $exitnow = true;
            echo $debug ? "<br/>NOW EXITING FINDERZ @ $newstarter " : "";
        }
        
        if ($newstarter < strlen($haystack) && $endnow !== true && $exitnow !== true) {
            echo $debug ? "<br/>re-iterating finderz for segment #$currentkey ..." : "";
            finderz($needle,$needlez,$haystack,$newstarter,$a,$antineedle,$antistarter);   
        }
        else {
            //echo "<br/>exiting finderz....<br/>\n";
            $newhaystack = $haystack;
            return;
        }
    }
    else {
        echo $debug ? "<br/> Finderz(): opening tag was not found" : "";
    }
    */
}


function filtermatched($input) {
    //file_put_contents("debug/startfilter.txt",print_r($input,true));
    //file_put_contents("debug/tester.txt","");
    global $discarded;
    /*echo "<pre>";
    print_r($input);
    echo "</pre>";*/
    foreach($input as $key=>$val) {
        $getmatches = array();
        if (setunknownvar('matches',$val)) {
            //echo "matches found in val, proceeding by setting getmatches var...";
            $getmatches = $val['matches'];
        }
        
        //echo "printing matches:<br/><pre>";
        //print_r($getmatches);
        //echo "</pre>";
        $singlematch = "";
        //echo "getmatches type:" . gettype($getmatches);
        if (is_array($getmatches) === false) { settype($getmatches,"array"); }
        //echo " - getmatches new type:" . gettype($getmatches) . "<br/>\n";
        $collectgoodmatches = count($getmatches);
        
        //echo "matches collected for segment #$key: $collectgoodmatches ....";
        
        foreach($getmatches as $k=>$v) {
            /*echo "<br/>printing matches:<br/><pre>";
            print_r($getmatches);
            echo "</pre><br/>";*/
            $mmm = $v['bolds']['matched'];
            //file_put_contents("debug/tester.txt",$mmm."\n",FILE_APPEND);
            if ($mmm == "no") {
                $collectgoodmatches--;
                //echo "about to unset match id [$key-$k] due to no match";
                unset($input[$key]['matches'][$k]);
            }
            else {
                if (floatval($v['bolds']['sensmatch']) <= 0.39999 ) { //&& floatval($v['bolds']['sensmatchrel']) < 0.95 ) { // && floatval($v['bolds']['sensmatchrel']) < 0.95) {
                    $collectgoodmatches--;
                    //$discarded[$key] = $val;
                    //echo "about to unset match id [$key-$k] due to under 0.39999...";
                    unset($input[$key]['matches'][$k]);
                }
            }
        }
        
        //echo "Remaining good matches collected for segment #$key: $collectgoodmatches <br/>";
        if ($collectgoodmatches == 0) {
            $discarded[$key]['phrase'] = $input[$key]['phrase'];
            $discarded[$key]['reason'] = "All matches were unreliable or of very low match.";
            unset($input[$key]);
        }
    }
    //array_values($input);
    //remove the segments with no matches and keep only the ones with a match, put the discarded ones in another array
    unset($key,$val,$getmatches);
    foreach($input as $key=>$val) {
        $getmatches = $val['matches'];
        if (count($getmatches) == 0) {
            unset($input[$key]);
        }
    }
    //array_values($input);
    /*
    $matched = array();
    foreach ($res as $key=>$val) {
        if ($val['matched'] == 'yes') {
            $matched[$id]['phrase'] = $val['phrase'];
            $matched[$id]['sensmatchrel'] = $val['sensmatchrel'];
        }
    }
    file_put_contents("debug/output-filtered.txt",print_r($matched,true));
    return $matched;
    */
    //file_put_contents("debug/output-filtered.txt",print_r($input,true));
    return $input;
}


function boldfinder($input,$sens=0.65) {
    global $sensitivity,$debug;
    $a = stripos($input,'<span class="st">');
    $b = stripos($input,'</span></div>',$a+strlen('<span class="st">')+1);
    $str = substr($input,$a+strlen('<span class="st">'),$b-($a+strlen('<span class="st">')));
    $c = stripos($str,'</span>');
    $d = stripos($str,'<b>...</b>');
    if ($d !== false) { $strx = substr($str,0,$d-($c+strlen('</span>'))+7); } //$c+strlen('</span>')
    else { $strx = substr($str,0,$b-($c+strlen('</span>'))+7); }
    $strx = str_ireplace("<em><em>","<em>",$strx);
    $strx = str_ireplace("</em></em>","</em>",$strx);
    $x = stripos($strx,"<span class=");
    $xx = stripos($strx,"</span><em>",$x);
    $xxx = stripos($strx,"<em>",$xx);
    $y = stripos($strx,'<b>...</b>');
    if (($x == 0 or ($x !== false && $xx>$x)) and $xxx-$xx == 7) {
        $strx = substr($strx,$xxx);
    }
    if ($debug) { $output['original_phrase'] = $strx; } 
    //echo "String from boldfinder: $strx <br/>\n";
    
    $xxx = phrasecleaner($strx);
    
    $phrasewords = explode(" ",trim($xxx['clean_normal']));
    $phrasewordsrel = explode(" ",trim($xxx['clean_rel']));
    
    $parser = new htmlParser(trim($xxx['clean_html']));
    $html = $parser->toArray();
    
    $totalmatchwords = 0;
    foreach ($html as $k=>$v) {
        if ($debug) {
            $output['chunks'][] = print_r($v,true);
        }
        $data = $v['innerHTML'];
        $matchwords = explode(" ",$data);
        $totalmatchwords = $totalmatchwords + count($matchwords);
    }
    //echo "Total match words: $totalmatchwords (ABS: ".count($phrasewords). "), (REL: ".count($phrasewordsrel).")<br/>\n";
    $sensmatch = $totalmatchwords / count($phrasewords);
    $sensmatchrel = $totalmatchwords/ count($phrasewordsrel);
    $temp = explode(" ",$strx);
    $wordsinphrase = count($temp);
    //echo $totalmatchwords."<br/>";
    if ($sensmatchrel >= $sens) { $matched = 'yes'; }
    else {
        $matched = 'no';
    }
    $output['phrase'] = substr($strx,0,256);
    if ($debug) { $output['processed_phrase'] = trim($xxx['clean_normal']); } 
    $output['sensmatch'] = $sensmatch;
    $output['sensmatchrel'] = $sensmatchrel;
    $output['matched'] = $matched;
    //file_put_contents("debug/output-res.txt",print_r($output,true));
    return $output;
    
}

function phrasecleaner($phrase) {
    $find = array(
        "<b>",
        "</b>",
        "...",
        "....",
        ",",
        ".",
        ";"
        );
    $tagx = array("<b>","</b>","...","....");
    $bb = stripos($phrase,"<em>");
    $aa = strripos($phrase,'</em>');
    $newphrase = substr($phrase,$bb+strlen("<em>"),$aa-$bb);
    //echo "Start phrase: ".$newphrase ."<br/>";
    $cleanphrasehtml = str_ireplace($tagx,"",$phrase);
    $cleanphrase = str_ireplace($find,"",$phrase);
    $cleanphraserel = str_ireplace($find,"",$newphrase);
    $output['clean_normal'] = $cleanphrase;
    $output['clean_html'] = $cleanphrasehtml;
    $output['clean_rel'] = $cleanphraserel;
    return $output;
}


function resultloader($url,$pr=false) {
    global $useragentfile, $version, $timetoconnect, $timetoreceive, $proxies;
    $useragents = file($useragentfile);
    //echo "Type of useragents var: ". gettype($useragents) ."<br/>\n";
    $totaluseragents = count($useragents);
    //echo "Count of total useragents: $totaluseragents <br/>\n";
    //echo "running url: $url <br/>\n";
    $pick = rand(0,$totaluseragents-1);
    $ua = $useragents[$pick];
    if (stripos($url,'wikilectures.eu') !== false) { $ua = "Plagiarism Checker/$version"; }
    //echo "ua used: $ua <br/>\n";
    // create a new cURL resource
    //echo "Running curl...<br/>";
    $ch = curl_init();
    // set URL and other appropriate options
    if ($pr===true) {
        $proxy = $proxies[array_rand($proxies)];
        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, TRUE);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        $timetoconnect =+ 5;
        $timetoreceive =+ 5;
        echo "<br/>Using proxy: $proxy <br/>";
    }
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, $ua);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timetoconnect);
    curl_setopt($ch, CURLOPT_TIMEOUT, $timetoreceive);
    // grab URL and pass the ouput to the variable
    $output = curl_exec($ch);
    // close cURL resource, and free up system resources
    curl_close($ch);
    return $output;
}
function removewikitags($input) {
$test = $input;
//$test = strip_tags($test);
$a = stripos($test,"<noinclude>");
//echo $a ? "true" : "false";
if ($a === false) { $a = strlen($input); }

$test = substr($test,0,$a);
$remove = array("## ","# ", "** ", "* ", "*","**","##","#");
$replace = " ";
$remove2 = "  ";
$replace2 = " ";
$test = str_replace($remove,$replace,$test);
$test = str_replace($remove2,$replace2,$test); 

$new = preg_replace("/\'\'\'(.*?)\'\'\'/ism","$1",$test);
//$new = preg_replace("/\[\[(.*?)\]\]/ism","$1",$new);
$new = preg_replace("/''(.*?)''/ism","$1",$new);
$new = preg_replace("/\[\[File:(.*?)\]\]|===(.*?)===|==(.*?)==|__TOC__|\# |\* /ism","\n",$new);
$new = preg_replace("/<sub>(.*?)<\/sub>|<sup>(.*?)<\/sup>|<ref>(.*?)<\/ref>|<nowiki>(.*?)<\/nowiki>|<source(.*?)<\/source>|\{\{(.*?)\}\}/ism","$1$2",$new);
$new = preg_replace("/{\|(.*?)\|\}/ism","",$new);
$new = preg_replace_callback(
                             "/\[\[(.*?)\]\]/ism",
                             create_function(
                                '$matches',
                                '
                                $a = preg_replace("/\[\[(.*?)\|/ism","",$matches[0]);
                                return preg_replace("/(.*?)\]\]/ism","$1",$a);
                                '
                             )
                             ,$new);
$new = preg_replace("/\[\[(.*?)\]\]/ism","$1",$new);
$new = preg_replace("/\[(.*?)\]/ism","$1",$new);
$new = str_replace("'''","",$new);
$new = str_replace("[[","",$new);
$new = str_replace("\n\n","\n",$new);
return $new;
}

function resultloaderz($url) {
    // create a new cURL resource
    //echo "Running curl...<br/>";
    $ch = curl_init();
    // set URL and other appropriate options
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.220 Safari/535.1");
    // grab URL and pass it to the browser
    $output = curl_exec($ch);
    // close cURL resource, and free up system resources
    curl_close($ch);
    //echo $output;
    //$outputz = simplexml_load_string($output);
    return $output;
}
function getdataz($article) {
    global $otherarticles;
    //require_once("htmlparser.php");
    //file_put_contents("debug/extractor.txt","\nloaded parser");
    $server = $_SERVER['SERVER_NAME'];
    if (stripos($server,"localhost")!== false or stripos($server,"127.0.0.1") !== false) {
        //echo $server;
        $server = "www.wikilectures.eu";
    }
    $url_extract = "http://$server/api.php?format=txtfm&action=query&export&exportnowrap&titles=$article";
    
    $data_extract = resultloader($url_extract,false);
    //file_put_contents("debug/wikirawxml.txt",$data_extract);
    //echo $data;
    //echo "<br/><br/>";
    $xmlarray = xml2array($data_extract);
    //print_r($xmlarray);
    $puretext = $xmlarray['mediawiki']['page']['revision']['text'];
    $data_extract = html_entity_decode($data_extract);
    //$aaa_extract = stripos($data_extract,"xml:space=\"preserve\"");
    if (stripos($puretext,"INSERTED ARTICLE") !== false)  {
        $aaaa = stripos($puretext,"[[Category:Inserted articles]]");
        $puretext = substr($puretext,$aaaa+30);
    }
    //file_put_contents("debug/raw-wiki.txt",$puretext);
    $matchnum = preg_match_all("/{{:(.*?)}}/ism",$puretext,$matches);
    $matchotherarticles = $matches[1];
    if (count($matchotherarticles) >0) {
        //echo "<br/>setting up otherarticles...";
        foreach($matchotherarticles as $k=>$v) {
            $otherarticles[] = $v;
        }
    }
    return removewikitags($puretext);

}

function the_time($comment) {
    global $timerz;
    $lasttime = end($timerz);
    $timenow = microtime(true);
    $timerz[] = $timenow;
    $diff = $timenow - $lasttime;
    print "<br/><i>$comment</i> - $timenow (+$diff)<br/>";
}

function time_estimator($newtime) {
    global $inittime, $lasttime, $currentkey, $totalqueries;
    //echo "<br/>Last time: $lasttime - New time: $newtime";
    $diff1  = $newtime - $inittime;
    $diff2 = $newtime - $lasttime;
    $elapsed = $diff1;
    //echo "<br/>So far: $diff1 - Last diff: $diff2";
    $averagesofar = $diff1/($currentkey+1);
    $averageperone = ($averagesofar + $diff2)/2;
    $howmanyleft = $totalqueries-($currentkey+1);
    $totaltimeleft = $howmanyleft * $averageperone;
    return human_time($totaltimeleft,$averageperone,$elapsed);
}

function human_time($input,$input2,$input3) {
    $seconds = round($input,0);
    $avg = round($input2,2);
    //$avg = strftime("%S secs",$avg);
    $elapsed = round($input3,0);
    $elapsed = strftime("%M mins %S secs",$elapsed);
    $output = strftime("%M mins %S secs",$seconds) . " - Time Elapsed: $elapsed - <i>".$avg."s per phrase</i>";
    return $output;
}

function printotherarticles() {
    global $otherarticles;
    $baseurl = $_SERVER['SCRIPT_NAME'];
    $server = $_SERVER['SERVER_NAME'];
    $a = strripos($baseurl,"/");
    $me = substr($baseurl,$a+1);
    $baseurl = $server . substr($baseurl,0,$a+1);
    $baseurl = $me;
    if (count($otherarticles) == 0) { return; }
    else {
        $xxx = "<ul style='font-size: 9pt;'>\n";
        foreach ($otherarticles as $k=>$v) {
            $x = $k+1;
            $vv = str_replace("_"," ",$v);
            $href = "<a href='$baseurl?article=$v' target='_blank' alt='Click to check!'>$vv</a>";
            $xxx .= "<li>$x: $href</li>\n";
        }
        $xxx .= "</ul>\n";
        $output = "<div><span style='font-size: 10pt;float:left; color: red;'><b>NOTE:</b> This article includes other articles that have to be checked separately:</span><br/>$xxx</div>";
        return $output;    
    }
}

function setunknownvar($index,$array) {
    if (array_key_exists($index,$array)) { return $array[$index]; }
    else { return false; }
}

function xml2array($contents, $get_attributes=1, $priority = 'tag') { 
    if(!$contents) return array(); 

    if(!function_exists('xml_parser_create')) { 
        //print "'xml_parser_create()' function not found!"; 
        return array(); 
    } 

    //Get the XML parser of PHP - PHP must have this module for the parser to work 
    $parser = xml_parser_create(''); 
    xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss 
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0); 
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1); 
    xml_parse_into_struct($parser, trim($contents), $xml_values); 
    xml_parser_free($parser); 

    if(!$xml_values) return;//Hmm... 

    //Initializations 
    $xml_array = array(); 
    $parents = array(); 
    $opened_tags = array(); 
    $arr = array(); 

    $current = &$xml_array; //Refference 

    //Go through the tags. 
    $repeated_tag_index = array();//Multiple tags with same name will be turned into an array 
    foreach($xml_values as $data) { 
        unset($attributes,$value);//Remove existing values, or there will be trouble 

        //This command will extract these variables into the foreach scope 
        // tag(string), type(string), level(int), attributes(array). 
        extract($data);//We could use the array by itself, but this cooler. 

        $result = array(); 
        $attributes_data = array(); 
         
        if(isset($value)) { 
            if($priority == 'tag') $result = $value; 
            else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
        } 

        //Set the attributes too. 
        if(isset($attributes) and $get_attributes) { 
            foreach($attributes as $attr => $val) { 
                if($priority == 'tag') $attributes_data[$attr] = $val; 
                else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr' 
            } 
        } 

        //See tag status and do the needed. 
        if($type == "open") {//The starting of the tag '<tag>' 
            $parent[$level-1] = &$current; 
            if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag 
                $current[$tag] = $result; 
                if($attributes_data) $current[$tag. '_attr'] = $attributes_data; 
                $repeated_tag_index[$tag.'_'.$level] = 1; 

                $current = &$current[$tag]; 

            } else { //There was another element with the same tag name 

                if(isset($current[$tag][0])) {//If there is a 0th element it is already an array 
                    $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result; 
                    $repeated_tag_index[$tag.'_'.$level]++; 
                } else {//This section will make the value an array if multiple tags with the same name appear together
                    $current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
                    $repeated_tag_index[$tag.'_'.$level] = 2; 
                     
                    if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
                        $current[$tag]['0_attr'] = $current[$tag.'_attr']; 
                        unset($current[$tag.'_attr']); 
                    } 

                } 
                $last_item_index = $repeated_tag_index[$tag.'_'.$level]-1; 
                $current = &$current[$tag][$last_item_index]; 
            } 

        } elseif($type == "complete") { //Tags that ends in 1 line '<tag />' 
            //See if the key is already taken. 
            if(!isset($current[$tag])) { //New Key 
                $current[$tag] = $result; 
                $repeated_tag_index[$tag.'_'.$level] = 1; 
                if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data; 

            } else { //If taken, put all things inside a list(array) 
                if(isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...

                    // ...push the new element into that array. 
                    $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result; 
                     
                    if($priority == 'tag' and $get_attributes and $attributes_data) { 
                        $current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data; 
                    } 
                    $repeated_tag_index[$tag.'_'.$level]++; 

                } else { //If it is not an array... 
                    $current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
                    $repeated_tag_index[$tag.'_'.$level] = 1; 
                    if($priority == 'tag' and $get_attributes) { 
                        if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
                             
                            $current[$tag]['0_attr'] = $current[$tag.'_attr']; 
                            unset($current[$tag.'_attr']); 
                        } 
                         
                        if($attributes_data) { 
                            $current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data; 
                        } 
                    } 
                    $repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken 
                } 
            } 

        } elseif($type == 'close') { //End of tag '</tag>' 
            $current = &$parent[$level-1]; 
        } 
    } 
     
    return($xml_array); 
}

function striposa($haystack, $needles=array(), $offset=0) {
        $chr = array();
        foreach($needles as $needle) {
                $res = stripos($haystack, $needle, $offset);
                if ($res !== false) $chr[$needle] = $res;
        }
        if(empty($chr)) return false;
        return min($chr);
}

/*
 OLD FUNCTIONS
===============

function boldfinder($phrase,$sens=0.65,$needle,$antineedle='') {
    global $threshold;
    //echo "BOLDFINDER CALLED! <BR/>\n";
    if ($antineedle == '') {
        $antineedle = str_replace("<","</",$needle);
    }
    $punctuation = array (
    ",",
    ".",
    ";"
    );
    $aa = strripos($phrase,$antineedle);
    $newphrase = substr($phrase,0,$aa);
    //echo "new phase: $newphrase \n";
    global $punctuation;
    $output = array();
    $find = array(
        "<b>",
        "</b>",
        "...",
        "....",
        ",",
        ".",
        ";",
        $antineedle,
        $needle
        );
    $tagx = array("<b>","</b>","...","....");
    //$tags = array($needle,$antineedle);
    //$find = array_merge($punctuation,$tags,$tagx);
    $cleanphrasehtml = str_ireplace($tagx,"",$phrase);
    $cleanphrase = str_ireplace($find,"",$phrase);
    $cleanphraserel = str_ireplace($find,"",$newphrase);
    $phrasewords = explode(" ",$cleanphrase);
    $phrasewordsrel = explode(" ",$cleanphraserel);
    $parser = new htmlParser($cleanphrasehtml);
    $html = $parser->toArray();
    //print_r($html);
    
    $totalmatchwords = 0;
    foreach ($html as $k=>$v) {
        $data = $v['innerHTML'];
        $matchwords = explode(" ",$data);
        $totalmatchwords = $totalmatchwords + count($matchwords);
    }
    //echo "Total match words: $totalmatchwords (".count($phrasewords). ")\n";
    $sensmatch = $totalmatchwords / count($phrasewords);
    $sensmatchrel = $totalmatchwords/ count($phrasewordsrel);
    $temp = explode(" ",$phrase);
    $wordsinphrase = count($temp);
    //echo $totalmatchwords."<br/>";
    if ($sensmatchrel >= $sens) { $matched = 'yes'; }
    else {
        $matched = 'no';
    }
    $output['phrase'] = substr($phrase,0,150); 
    $output['sensmatch'] = $sensmatch;
    $output['sensmatchrel'] = $sensmatchrel;
    $output['matched'] = $matched;
    //file_put_contents("debug/output-res.txt",print_r($output,true));
    return $output;
}

function breakup($input,$words=10) {
    $input = unbullet($input);
    //echo $input;
    $wordsarray = explode(" ",$input);
    
    $segments = array();
    $counter = 1;
    $countw = 1;
    foreach ($wordsarray as $key=>$val) {
        if ($countw == $words) {
            $counter++;
            $segments[$counter-1] = $segments[$counter-1] . " $val";
            $countw=1;
        }
        else {
            $segments[$counter-1] = $segments[$counter-1] . " $val";
            $countw++;
        }
        //echo "Word counter: $countw  -  Segment counter: $counter <br/>";
    }
    unset($key,$val);
    
    #white space removal
    foreach ($segments as $key=>$val) {
        $valz = trim($val);
        $segments[$key] = $valz;
    }
    
    array_values($segments);
    //echo "<pre>";print_r($segments);echo "</pre>";
    
    return $segments;
}
 
*/

?>