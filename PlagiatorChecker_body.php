<?php
class PlagiatorChecker extends SpecialPage {
	function __construct() {
		parent::__construct( 'PlagiatorChecker' );
		wfLoadExtensionMessages('PlagiatorChecker');
	}

	function execute($article) {
		global $wgRequest, $wgOut;

		$this->setHeaders();

		if(!$article) {
			$article = $wgRequest->getText('article');
		}

 		if(!$article) {
			$wgOut->addWikiText(wfMsg('plagiatorchecker-desc'));
			$wgOut->addHTML('<form action="#">' . wfMsg("pgc-articlename") . ': <input type="text" id="article" name="article" /><input type="submit" value="OK" /></form>');
		} else {
			$wgOut->addHTML('<h2>Checking article:' . $article . '</h2>');
			$wgOut->addHTML('<iframe width="80%" height="700" src="http://www.wikilectures.eu/extensions/PlagiatorChecker/copyvio-checker-wiki.php?article=' . urlencode($article) . '"></iframe>');
		}
	}
}

?>
