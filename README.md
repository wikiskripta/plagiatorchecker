# PlagiatorChecker

Extension created by Paris Charilaou for WikiLectures

## Description

* Version 2.0.0
* Doesn't work

## Installation

* Download and place the extension to your _/extensions/_ folder.
* Add required cron jobs.
* Add the following code to your LocalSettings.php: `require_once( "$IP/extensions/PlagiatorChecker/PlagiatorChecker.php" )`;

## Authors and license

* Paris Charilaou, [Josef Martiňák](https://www.wikiskripta.eu/w/User:Josmart)
* MIT License, Copyright (c) 2018 First Faculty of Medicine, Charles University
