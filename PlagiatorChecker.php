<?php
# Alert the user that this is not a valid entry point to MediaWiki if they try to access the special pages file directly.
if (!defined('MEDIAWIKI')) {
echo <<<EOT
	To install this extension, put the following line in LocalSettings.php:
	require_once( "\$IP/extensions/PlagiatorChecker/PlagiatorChecker.php\" );
EOT;
exit( 1 );
}

$wgExtensionCredits['specialpage'][] = array(
	'name' => 'PlagiatorChecker',
	'author' => 'Paris Charilaou',
	'url' => "http://www.wikilectures.eu",
	'description' => 'Checks possible plagiarisms in Google Search.',
	'descriptionmsg' => 'plagiatorchecker-desc',
	'version' => '2.0.0'
);

$dir = dirname(__FILE__) . '/';


$wgAutoloadClasses['PlagiatorChecker'] = $dir . 'PlagiatorChecker_body.php'; # Tell MediaWiki to load the extension body.
$wgExtensionMessagesFiles['PlagiatorChecker'] = $dir . 'PlagiatorChecker.i18n.php';
$wgSpecialPages['PlagiatorChecker'] = 'PlagiatorChecker'; # Let MediaWiki know about your new special page.


?>
